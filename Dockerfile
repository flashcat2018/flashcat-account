FROM adoptopenjdk/openjdk11-openj9

ADD ./target/accounts-1.0.jar /app/app.jar
CMD java -jar /app/app.jar 

EXPOSE 8080