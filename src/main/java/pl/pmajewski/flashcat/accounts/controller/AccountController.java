package pl.pmajewski.flashcat.accounts.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.apachecommons.CommonsLog;
import pl.pmajewski.flashcat.accounts.auth.Auth;
import pl.pmajewski.flashcat.accounts.dto.AccountDTO;
import pl.pmajewski.flashcat.accounts.dto.ChangePasswordDTO;
import pl.pmajewski.flashcat.accounts.dto.VerifyDTO;
import pl.pmajewski.flashcat.accounts.service.AccountService;
import pl.pmajewski.flashcat.accounts.service.impl.security.AccountServiceAccessProxy;

@RestController
@RequestMapping("accounts")
@CommonsLog
public class AccountController {
	
	private AccountService accountService;
	private Auth auth;

	public AccountController(AccountServiceAccessProxy accountService, Auth auth) {
		this.accountService = accountService;
		this.auth = auth;
	}
	
	/**
	 * 
	 * Append token as header 'authorization'
	 * 
	 * @param authorization
	 * @return AccountPojo
	 */
	@GetMapping("token")
	public AccountDTO getByToken() {
		log.info("["+auth.getUserId()+"] Get account details by token");
		return accountService.getById(auth.getUserId());
	}
	
	@GetMapping("{id}")
	public AccountDTO getById(@PathVariable Long id) {
		return accountService.getById(id);
	}
	
	@PostMapping("verify")
	public AccountDTO getByUsernameAndPassword(@RequestBody VerifyDTO body) {
		return accountService.verify(body);
	}
	
	@DeleteMapping("{id}")
	public void removeAccount(@PathVariable Long id) {
		log.info("["+auth.getUserId()+"] Remove account "+id);
		accountService.remove(id);
	}
	
	@PostMapping("{id}/changePassword")
	public void changePassword(@PathVariable Long id, @RequestBody ChangePasswordDTO body) {
		log.info("["+auth.getUserId()+"] Change password "+body);
		accountService.changePassword(id, body);
	}
}
