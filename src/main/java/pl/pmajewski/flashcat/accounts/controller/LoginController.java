package pl.pmajewski.flashcat.accounts.controller;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pl.pmajewski.flashcat.accounts.config.GlobalProperties;
import pl.pmajewski.flashcat.accounts.dto.JWTResponseDTO;
import pl.pmajewski.flashcat.accounts.dto.LoginResponseDTO;
import pl.pmajewski.flashcat.accounts.dto.ResetPasswordDTO;
import pl.pmajewski.flashcat.accounts.service.AccountService;
import pl.pmajewski.flashcat.accounts.service.LoginExecutor;
import pl.pmajewski.flashcat.accounts.service.impl.security.AccountServiceAccessProxy;

@RestController
public class LoginController {

	private LoginExecutor loginExecutor;
	private AccountService accountService;
	
	public LoginController(LoginExecutor loginService, AccountServiceAccessProxy accountService) {
		this.loginExecutor = loginService;
		this.accountService = accountService;
	}

	@PostMapping("/login")
	public LoginResponseDTO login(@RequestHeader("username") String username, @RequestHeader("password") String password, HttpServletResponse response) {
		LoginResponseDTO body = loginExecutor.checkAuth(username, password);
		response.addHeader("Authorization", body.token);
		return body;
	}
	
	@GetMapping("/jwt/settings")
	public JWTResponseDTO getJWTToken() {
		return new JWTResponseDTO(GlobalProperties.tokenSecret, GlobalProperties.tokenLifetime);
	}
	
	@PostMapping("/login/resetPasswordRequest")
	public void resetPasswordRequest(@RequestParam String email) {
		accountService.resetPasswordRequest(email);
	}
	
	@PostMapping("/login/resetPassword")
	public void resetPassword(@Valid @RequestBody ResetPasswordDTO body) {
		accountService.resetPassword(body);
	}
}
