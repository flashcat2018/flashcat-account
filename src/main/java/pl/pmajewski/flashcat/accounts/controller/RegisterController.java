package pl.pmajewski.flashcat.accounts.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.apachecommons.CommonsLog;
import pl.pmajewski.flashcat.accounts.dto.AccountDTO;
import pl.pmajewski.flashcat.accounts.dto.RegisterDTO;
import pl.pmajewski.flashcat.accounts.service.RegisterService;

@RestController
@CommonsLog
public class RegisterController {

	private RegisterService registerService;

	public RegisterController(RegisterService registerService) {
		this.registerService = registerService;
	}

	@PostMapping(value = "/register")
	@ResponseStatus(value = HttpStatus.CREATED)
	public AccountDTO register(@Valid @RequestBody RegisterDTO body) {
		log.info("[REGISTER] "+body);
		AccountDTO account = registerService.register(body);
		return account;
	}
}
