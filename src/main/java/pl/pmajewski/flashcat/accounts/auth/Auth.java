package pl.pmajewski.flashcat.accounts.auth;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import lombok.Getter;

@Component
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class Auth {

	@Autowired
	private HttpServletRequest request;
	
	@Getter
	private Long userId;
	
	@PostConstruct
	private void init() {
		 userId = Long.parseLong(request.getHeader("userId"));
	}
}
