package pl.pmajewski.flashcat.accounts.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class GlobalProperties {

	public static String tokenSecret;
	public static Long tokenLifetime;
	public static String forgottenPasswordEmailTemplateName;
	public static String passwordResetUrl;
	public static Long resetTokenLifetime;
	

	@Value("${token.secret}")
	public void setTokenSecret(String value) {
		GlobalProperties.tokenSecret = value;
	}
	
	@Value("${token.lifetime}")
	public void setTokenLifetime(String value) {
		GlobalProperties.tokenLifetime = Long.parseLong(value);
	}
	
	@Value("${email.template.forgotten-password.name}")
	public void setForgottenPasswordEmailTemplateName(String value) {
		GlobalProperties.forgottenPasswordEmailTemplateName = value;
	}
	
	@Value("${web.password-reset-url}")
	public void setPasswordResetUrl(String value) {
		GlobalProperties.passwordResetUrl = value;
	}
	
	@Value("${reset-token-lifetime}")
	public void setResetTokenLifetime(String value) {
		GlobalProperties.resetTokenLifetime = Long.parseLong(value);
	}
}
