package pl.pmajewski.flashcat.accounts.service;

import pl.pmajewski.flashcat.accounts.dto.LoginResponseDTO;

public interface LoginExecutor {
	
	LoginResponseDTO checkAuth(String username, String password);
}
