package pl.pmajewski.flashcat.accounts.service;

import pl.pmajewski.flashcat.accounts.dto.AccountDTO;
import pl.pmajewski.flashcat.accounts.dto.ChangePasswordDTO;
import pl.pmajewski.flashcat.accounts.dto.ResetPasswordDTO;
import pl.pmajewski.flashcat.accounts.dto.VerifyDTO;

public interface AccountService {
	
	AccountDTO getById(Long id);
	
	void remove(Long accoutnId);
	
	void changePassword(Long accountId, ChangePasswordDTO body);
	
	AccountDTO verify(VerifyDTO body);
	
	void resetPasswordRequest(String email);

	void resetPassword(ResetPasswordDTO body);
}
