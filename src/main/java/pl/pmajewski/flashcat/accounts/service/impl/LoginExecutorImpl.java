package pl.pmajewski.flashcat.accounts.service.impl;

import java.util.Date;
import java.util.UUID;

import org.springframework.stereotype.Component;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import pl.pmajewski.flashcat.accounts.config.GlobalProperties;
import pl.pmajewski.flashcat.accounts.dto.AccountDTO;
import pl.pmajewski.flashcat.accounts.dto.LoginResponseDTO;
import pl.pmajewski.flashcat.accounts.dto.VerifyDTO;
import pl.pmajewski.flashcat.accounts.service.AccountService;
import pl.pmajewski.flashcat.accounts.service.LoginExecutor;
import pl.pmajewski.flashcat.accounts.service.impl.security.AccountServiceAccessProxy;

@Component
public class LoginExecutorImpl implements LoginExecutor {
	
	private AccountService accountService;
	
	public LoginExecutorImpl(AccountServiceAccessProxy accountService) {
		this.accountService = accountService;
	}

	@Override
	public LoginResponseDTO checkAuth(String username, String password) {
		AccountDTO account = accountService.verify(new VerifyDTO(username, password));
		
		String token = Jwts.builder()
				.setId(UUID.randomUUID().toString())
				.setSubject(account.id.toString())
				.setExpiration(new Date(System.currentTimeMillis()+GlobalProperties.tokenLifetime))
				.signWith(SignatureAlgorithm.HS512, GlobalProperties.tokenSecret)
				.compact();
		
		return LoginResponseDTO.builder()
			.id(account.id)
			.username(account.username)
			.registerTime(account.registerTime)
			.token(token)
			.build();
	}
}
