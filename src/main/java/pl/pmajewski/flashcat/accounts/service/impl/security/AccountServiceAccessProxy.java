package pl.pmajewski.flashcat.accounts.service.impl.security;

import org.springframework.stereotype.Component;

import pl.pmajewski.flashcat.accounts.auth.Auth;
import pl.pmajewski.flashcat.accounts.dto.AccountDTO;
import pl.pmajewski.flashcat.accounts.dto.ChangePasswordDTO;
import pl.pmajewski.flashcat.accounts.dto.ResetPasswordDTO;
import pl.pmajewski.flashcat.accounts.dto.VerifyDTO;
import pl.pmajewski.flashcat.accounts.exception.UnauthorizedAccessException;
import pl.pmajewski.flashcat.accounts.service.AccountService;
import pl.pmajewski.flashcat.accounts.service.impl.AccountServiceImpl;

@Component
public class AccountServiceAccessProxy implements AccountService {

	private AccountServiceImpl accountService;
	private Auth auth;
	
	public AccountServiceAccessProxy(AccountServiceImpl accountService, Auth auth) {
		this.accountService = accountService;
		this.auth = auth;
	}

	@Override
	public AccountDTO getById(Long id) {
		if(!auth.getUserId().equals(id)) {
			throw new UnauthorizedAccessException();
		}
		
		return accountService.getById(id);
	}

	@Override
	public void remove(Long accoutnId) {
		if(!auth.getUserId().equals(accoutnId)) {
			throw new UnauthorizedAccessException();
		}
		
		accountService.remove(accoutnId);
	}

	@Override
	public void changePassword(Long accountId, ChangePasswordDTO body) {
		if(!auth.getUserId().equals(accountId)) {
			throw new UnauthorizedAccessException();
		}
		
		accountService.changePassword(accountId, body);
	}

	@Override
	public AccountDTO verify(VerifyDTO body) {
		return accountService.verify(body);
	}

	@Override
	public void resetPasswordRequest(String email) {
		accountService.resetPasswordRequest(email);
	}

	@Override
	public void resetPassword(ResetPasswordDTO body) {
		accountService.resetPassword(body);
	}
}
