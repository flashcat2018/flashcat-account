package pl.pmajewski.flashcat.accounts.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.pmajewski.flashcat.accounts.dto.AccountDTO;
import pl.pmajewski.flashcat.accounts.dto.RegisterDTO;
import pl.pmajewski.flashcat.accounts.exception.AccountExistsExeption;
import pl.pmajewski.flashcat.accounts.model.Account;
import pl.pmajewski.flashcat.accounts.repository.AccountRepository;
import pl.pmajewski.flashcat.accounts.service.RegisterService;
import pl.pmajewski.flashcat.accounts.utils.GeneratedPassword;
import pl.pmajewski.flashcat.accounts.utils.PasswordGenerator;

@Service
@Transactional
public class RegisterServiceImpl implements RegisterService {
	
	@Autowired
	private AccountRepository accountRepository;
	@Autowired
	private PasswordGenerator generator;
	
	@Override
	public AccountDTO register(RegisterDTO value) {
		if(accountExist(value.username)) {
			throw new AccountExistsExeption("Account related with username: "+value.username+" exists.");
		}
		
		Account account = new Account();
		account.setUsername(value.username);
		GeneratedPassword generatePassword = generator.generatePassword(value.password);
		account.setPassword(generatePassword.getPasswordHashed());
		account.setPasswordSalt(generatePassword.getSalt());
		
		accountRepository.save(account);
		
		return account.getPojo();
	}
	
	private boolean accountExist(String email) {
		return !accountRepository.findByUsername(email).isEmpty();
	}
}
