package pl.pmajewski.flashcat.accounts.service.impl;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.pmajewski.flashcat.accounts.client.EmailClient;
import pl.pmajewski.flashcat.accounts.config.GlobalProperties;
import pl.pmajewski.flashcat.accounts.dto.AccountDTO;
import pl.pmajewski.flashcat.accounts.dto.ChangePasswordDTO;
import pl.pmajewski.flashcat.accounts.dto.ResetPasswordDTO;
import pl.pmajewski.flashcat.accounts.dto.TemplateEmailBody;
import pl.pmajewski.flashcat.accounts.dto.VerifyDTO;
import pl.pmajewski.flashcat.accounts.exception.AccountNotFoundException;
import pl.pmajewski.flashcat.accounts.exception.InvalidInputDataException;
import pl.pmajewski.flashcat.accounts.exception.UserNotFoundException;
import pl.pmajewski.flashcat.accounts.model.Account;
import pl.pmajewski.flashcat.accounts.model.PasswordResetToken;
import pl.pmajewski.flashcat.accounts.repository.AccountRepository;
import pl.pmajewski.flashcat.accounts.service.AccountService;
import pl.pmajewski.flashcat.accounts.utils.GeneratedPassword;
import pl.pmajewski.flashcat.accounts.utils.PasswordGenerator;
import pl.pmajewski.flashcat.accounts.utils.ResetTokenGenerator;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {

	private AccountRepository accountRepository;
	private PasswordGenerator passwordGenerator;
	private EmailClient emailClient;

	public AccountServiceImpl(AccountRepository accountRepository, PasswordGenerator passwordGenerator, EmailClient emailClient) {
		this.accountRepository = accountRepository;
		this.passwordGenerator = passwordGenerator;
		this.emailClient = emailClient;
	}

	@Override
	public AccountDTO getById(Long id) {
		return accountRepository.findById(id)
			.orElseThrow(() -> new AccountNotFoundException("Account with id="+id+" not found"))
			.getPojo();
	}

	@Override
	public void remove(Long accoutnId) {
		accountRepository.deleteById(accoutnId);
	}

	@Override
	public void changePassword(Long id, ChangePasswordDTO body) {
		if(!body.password.equals(body.passwordConfirm)) {
			throw new InvalidInputDataException("Invalid password. Confirm password isn't the same as password.");
		}
		
		Account account = accountRepository.findById(id).orElseThrow(() -> new AccountNotFoundException("Account with id="+id+" not found"));
		GeneratedPassword pass = passwordGenerator.generatePassword(body.password);
		account.setPassword(pass.getPasswordHashed());
		account.setPasswordSalt(pass.getSalt());
		accountRepository.save(account);
	}
	
	@Override
	public AccountDTO verify(VerifyDTO body) { 
		Optional<Account> account = accountRepository.findByUsername(body.username);
		if(!account.isPresent() || !passwordGenerator.verifyPassword(body.password, account.get().getPassword(), account.get().getPasswordSalt())) {
			throw new UserNotFoundException("User not found. Invalid input data.");
		}
		return account.get().getPojo();
	}

	@Override
	public void resetPasswordRequest(String email) {
		Account account = accountRepository.findByUsername(email)
			.orElseThrow(() -> new AccountNotFoundException("Account related to "+email+" not found."));
	
		PasswordResetToken token = new PasswordResetToken(account, ResetTokenGenerator.getInstance().generateToken());
		String username = account.getFirstname() != null ? account.getFirstname() : "";
		TemplateEmailBody body = new TemplateEmailBody(account.getUsername(), "username", username, "activationLink", GlobalProperties.passwordResetUrl.replace("{token}", token.getToken()));
		emailClient.sendEmail(GlobalProperties.forgottenPasswordEmailTemplateName, body);
		account.getPasswordResetTokens().add(token);
	}
	
	@Override
	public void resetPassword(ResetPasswordDTO body) {
		Account account = accountRepository.findByUsername(body.email)
				.orElseThrow(() -> new AccountNotFoundException("Account related to "+body.email+" not found"));
	
		PasswordResetToken token = account.getPasswordResetTokens().stream()
				.filter(i -> isActiveResetToken(i, body.token)).findFirst()
				.orElseThrow(() -> new InvalidInputDataException("Invalid input data"));
		
		changePassword(account.getId(), new ChangePasswordDTO(body.password, body.password));
		token.setUsageTimestamp(LocalDateTime.now());
	}
	
	private boolean isActiveResetToken(PasswordResetToken token, String expectedToken) {
		return token.getUsageTimestamp() == null
				&& expectedToken != null
				&& !expectedToken.isEmpty()
				&& token.getToken().trim().equals(expectedToken.trim()) 
				&& token.getInsertTimestamp()
					.plusSeconds(GlobalProperties.resetTokenLifetime)
					.isAfter(LocalDateTime.now());
	}
}
