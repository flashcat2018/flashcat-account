package pl.pmajewski.flashcat.accounts.service;

import pl.pmajewski.flashcat.accounts.dto.AccountDTO;
import pl.pmajewski.flashcat.accounts.dto.RegisterDTO;

public interface RegisterService {
	
	AccountDTO register(RegisterDTO value);
}
