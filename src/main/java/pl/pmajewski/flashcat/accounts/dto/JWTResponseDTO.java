package pl.pmajewski.flashcat.accounts.dto;

import lombok.AllArgsConstructor;
import lombok.ToString;

@ToString
@AllArgsConstructor
public class JWTResponseDTO {

	public String token;
	public Long lifetime;
}
