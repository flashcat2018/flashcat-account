package pl.pmajewski.flashcat.accounts.dto;

import java.time.LocalDateTime;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class RegisterDTO {

	@NotNull(message = "Required")
	@Email(message = "Invalid email")
	@Size(max = 256, message = "Username should be less than 256")
	public String username;
	
	@NotNull(message = "Required")
	@Size(min = 8, max = 128, message = "Username should be between 8 and 128")	
	public String password;
	
	public String firstname;
	public String lastname;
	public LocalDateTime birthday;
}
