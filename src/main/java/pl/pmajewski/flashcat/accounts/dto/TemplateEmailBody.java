package pl.pmajewski.flashcat.accounts.dto;

import java.util.HashMap;
import java.util.Map;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class TemplateEmailBody {
	
	public String to;
	public Map<String, String> params;
	
	public TemplateEmailBody(String to, String... params) {
		this.to = to;
		this.params = infToParams(params);
	}
	
	private Map<String, String> infToParams(String... params) {
		Map<String, String> result = new HashMap<>();
		
		for(int i=0; i < params.length; i++) {
			String key = params[i];
			String value = params[++i];
			result.put(key, value);
		}
		
		return result;
	}
	
}
