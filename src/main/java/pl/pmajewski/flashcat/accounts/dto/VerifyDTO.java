package pl.pmajewski.flashcat.accounts.dto;

import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@NoArgsConstructor
public class VerifyDTO {
	
	public String username;
	public String password;
	
	public VerifyDTO(String username, String password) {
		this.username = username;
		this.password = password;
	}
}
