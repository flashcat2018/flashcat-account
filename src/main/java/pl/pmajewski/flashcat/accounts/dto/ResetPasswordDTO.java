package pl.pmajewski.flashcat.accounts.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ResetPasswordDTO {
	
	public String email;
	public String token;
	
	@NotNull(message = "Required")
	@Size(min = 8, max = 128, message = "Username should be between 8 and 128")	
	public String password;
}
