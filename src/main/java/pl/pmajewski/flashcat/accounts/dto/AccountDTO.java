package pl.pmajewski.flashcat.accounts.dto;

import java.time.LocalDateTime;

import lombok.ToString;

@ToString
public class AccountDTO {
	
	public Long id;
	public String username;
	public LocalDateTime registerTime;
}
