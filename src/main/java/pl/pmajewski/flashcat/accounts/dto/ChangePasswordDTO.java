package pl.pmajewski.flashcat.accounts.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ChangePasswordDTO {
	
	public String password;
	public String passwordConfirm;
}
