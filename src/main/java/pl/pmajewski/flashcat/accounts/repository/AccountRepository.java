package pl.pmajewski.flashcat.accounts.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import pl.pmajewski.flashcat.accounts.model.Account;

public interface AccountRepository extends CrudRepository<Account, Long> {

	@Query("select a from Account a where upper(a.username) = upper(:username)")
	Optional<Account> findByUsername(@Param("username") String username);
}
