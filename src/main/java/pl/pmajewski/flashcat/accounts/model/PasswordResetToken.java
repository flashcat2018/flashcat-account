package pl.pmajewski.flashcat.accounts.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(schema = "account", name = "password_reset_token")
@Data
@EqualsAndHashCode(of = "id")
@ToString(exclude = "account")
@NoArgsConstructor
public class PasswordResetToken {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column
	private String token;
	
	@Column(name = "insert_timestamp")
	private LocalDateTime insertTimestamp;

	@Column(name = "usage_timestamp")
	private LocalDateTime usageTimestamp;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private Account account;
	
	public PasswordResetToken(Account account, String token) {
		this.account = account;
		this.token = token;
	}
	
	@PrePersist
	protected void prePersist() {
		this.insertTimestamp = LocalDateTime.now();
	}
}
