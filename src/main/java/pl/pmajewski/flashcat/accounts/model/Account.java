package pl.pmajewski.flashcat.accounts.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import pl.pmajewski.flashcat.accounts.dto.AccountDTO;

@Entity
@Table(schema = "account", name = "users")
@Data
@EqualsAndHashCode(of = "id")
@ToString(exclude = {"passwordResetTokens"})
public class Account {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column
	private String username;
	
	@Column(length = 512)
	private String password;
	
	@Column(name = "password_salt", length = 512)
	private String passwordSalt;
	
	@Column
	private String firstname;
	
	@Column
	private String lastname;
	
	@Column
	private LocalDateTime birthday;
	
	@Column(name = "insert_timestamp")
	private LocalDateTime insertTimestamp;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "account")
	private List<PasswordResetToken> passwordResetTokens;
	
	public Account() { }
	
	public Account(Long accountId) {
		this.id = accountId;
	}
	
	public AccountDTO getPojo() {
		AccountDTO p = new AccountDTO();
		
		p.id = this.id;
		p.registerTime = this.insertTimestamp;
		p.username = this.username;
		
		return p;
	}
	
	@PrePersist
	private void prePersist() {
		this.insertTimestamp = LocalDateTime.now();
	}
}
