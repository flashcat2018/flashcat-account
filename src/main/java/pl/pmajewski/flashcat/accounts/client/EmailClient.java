package pl.pmajewski.flashcat.accounts.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import pl.pmajewski.flashcat.accounts.dto.TemplateEmailBody;

@FeignClient(name = "email-service")
public interface EmailClient {

	@PostMapping("/email/send/template/{templateName}")
	void sendEmail(@PathVariable("templateName") String templateName, @RequestBody TemplateEmailBody body);
	
}
