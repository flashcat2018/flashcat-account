package pl.pmajewski.flashcat.accounts.utils;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;
import java.util.stream.IntStream;

import org.springframework.stereotype.Component;

import lombok.extern.apachecommons.CommonsLog;

@Component
@CommonsLog
public class PasswordGenerator {
	
	private static final String LETTERS = "abcdefghijklouprstwxyzABCDEFGHIJKLMNOUPRSTWXYZ123456789";
	
	public boolean verifyPassword(String typedPassword, String hashedPassword, String salt) {
		return hashPassword(typedPassword, salt).equals(hashedPassword);
	}
	
	public GeneratedPassword generatePassword(String password) {
		String salt = generateSalt();
		String hashPassword = hashPassword(password, salt);	
		
		return new GeneratedPassword(hashPassword, salt);
	}
	
	public GeneratedPassword generatePassword(String password, String salt) {
		String hashPassword = hashPassword(password, salt);
		return new GeneratedPassword(hashPassword, salt);
	}
	
	private String hashPassword(String password, String salt) {
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-512");;	
			digest.update(salt.getBytes(StandardCharsets.UTF_8));
			byte[] bytes = digest.digest(password.getBytes(StandardCharsets.UTF_8));
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < bytes.length; i++) {
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}
			return sb.toString();			
		} catch (NoSuchAlgorithmException e) {
			log.error(e);
			throw new RuntimeException(e);
		}
	}

	private String generateSalt() {
		final StringBuilder sb = new StringBuilder();
		final Random rand = new Random();
		IntStream.range(0, 512).forEach(i -> {
			sb.append(LETTERS.charAt(rand.nextInt(LETTERS.length())));
		});
		
		return sb.toString();
	}
	
	public static void main(String... args) {
		PasswordGenerator pg = new PasswordGenerator();
		GeneratedPassword generated = pg.generatePassword("user3");
		System.out.println(generated.getPasswordHashed()+" | "+generated.getSalt());
	}
}
