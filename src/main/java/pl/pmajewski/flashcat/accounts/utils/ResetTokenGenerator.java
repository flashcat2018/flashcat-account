package pl.pmajewski.flashcat.accounts.utils;

import java.util.Random;
import java.util.stream.IntStream;

public class ResetTokenGenerator {

	private static final String LETTERS = "abcdefghijklouprstwxyzABCDEFGHIJKLMNOUPRSTWXYZ123456789";
	
	private static ResetTokenGenerator instance;
	
	private ResetTokenGenerator() { }
	
	public static ResetTokenGenerator getInstance() {
		if(instance == null) {
			synchronized (ResetTokenGenerator.class) {
				instance = new ResetTokenGenerator();
			}
		}
		
		return instance;
	}
	
	public String generateToken() {
		final StringBuilder sb = new StringBuilder();
		final Random rand = new Random();
		IntStream.range(0, 256).forEach(i -> {
			sb.append(LETTERS.charAt(rand.nextInt(LETTERS.length())));
		});
		
		return sb.toString();
	}
}
