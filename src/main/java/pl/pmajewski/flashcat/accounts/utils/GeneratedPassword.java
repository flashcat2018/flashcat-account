package pl.pmajewski.flashcat.accounts.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class GeneratedPassword {

	private String passwordHashed;
	private String salt;
	
}
