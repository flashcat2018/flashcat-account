package pl.pmajewski.flashcat.accounts.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class AccountExistsExeption extends RuntimeException {

	public AccountExistsExeption() {
		super();
	}

	public AccountExistsExeption(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public AccountExistsExeption(String message, Throwable cause) {
		super(message, cause);
	}

	public AccountExistsExeption(String message) {
		super(message);
	}

	public AccountExistsExeption(Throwable cause) {
		super(cause);
	}
}
