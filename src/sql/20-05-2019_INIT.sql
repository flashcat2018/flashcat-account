-- USERS
DROP TABLE IF EXISTS public.users CASCADE;
CREATE TABLE users (
    id bigserial NOT NULL,
    birthday timestamp without time zone,
    firstname character varying(255),
    insert_timestamp timestamp without time zone,
    lastname character varying(255),
    password character varying(512),
    password_salt character varying(512),
    username character varying(255)
);

ALTER TABLE IF EXISTS users ADD CONSTRAINT users_pkey PRIMARY KEY (id);

CREATE INDEX username_password_idx
    ON public.users USING btree
    (username COLLATE pg_catalog."default", password COLLATE pg_catalog."default")
    TABLESPACE pg_default;
