CREATE SCHEMA account;

CREATE TABLE account.password_reset_token
(
    id bigserial,
    token character varying,
    insert_timestamp timestamp without time zone,
    usage_timestamp timestamp without time zone,
    user_id bigint NOT NULL,
    PRIMARY KEY (id)
);