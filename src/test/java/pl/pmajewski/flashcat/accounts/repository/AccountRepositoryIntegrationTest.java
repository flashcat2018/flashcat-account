package pl.pmajewski.flashcat.accounts.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;

import pl.pmajewski.flashcat.accounts.model.Account;

@SpringBootTest
@TestPropertySource(locations="classpath:bootstrap-test.yml")
@ActiveProfiles("test")
class AccountRepositoryIntegrationTest {

	@Autowired
	AccountRepository accountRepository;
	
	@Test
	void findAll() {
		long findAll = accountRepository.count();
		assertThat(findAll).isEqualTo(3l);
	}
	
	@Test
	void findByUsername() {
		Optional<Account> account = accountRepository.findByUsername("user1@flashcat.io");
		assertThat(account.isPresent())
			.isTrue();
	}
}
