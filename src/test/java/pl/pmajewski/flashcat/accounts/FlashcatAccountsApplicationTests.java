package pl.pmajewski.flashcat.accounts;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;

@SpringBootTest
@TestPropertySource(locations="classpath:bootstrap-test.yml")
@ActiveProfiles("test")
public class FlashcatAccountsApplicationTests {

	@Test
	public void contextLoads() {
	}

}

