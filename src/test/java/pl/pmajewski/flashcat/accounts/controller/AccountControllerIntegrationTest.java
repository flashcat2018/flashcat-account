package pl.pmajewski.flashcat.accounts.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import pl.pmajewski.flashcat.accounts.auth.Auth;
import pl.pmajewski.flashcat.accounts.dto.ChangePasswordDTO;
import pl.pmajewski.flashcat.accounts.dto.VerifyDTO;
import pl.pmajewski.flashcat.accounts.repository.AccountRepository;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations="classpath:bootstrap-test.yml")
@ActiveProfiles("test")
class AccountControllerIntegrationTest {

	private static final String TEST_USERNAME = "user1@flashcat.io";
	private static final String TEST_PASSWORD = "user1";
	private static final Integer TEST_USER_ID = 1;
	
	
	@Autowired
	private AccountRepository accountRepo;
	@Autowired
	private TestRestTemplate rest;
	
	@MockBean
	private Auth auth;
	
	@BeforeEach
	void setUpAuth() {
		when(auth.getUserId()).thenReturn(TEST_USER_ID.longValue());
	}
	
	@Test
	void getByTokenTest() {
		ResponseEntity<String> response = rest.getForEntity("/accounts/token", String.class);
		assertThat(response.getStatusCode().is2xxSuccessful()).isTrue();
		
		DocumentContext context = JsonPath.parse(response.getBody());
		assertThat((String) context.read("$.username"))
			.isEqualTo(TEST_USERNAME);
	}
	
	@Test
	void getByIdTest() {
		ResponseEntity<String> response = rest.getForEntity("/accounts/"+TEST_USER_ID, String.class);
		assertThat(response.getStatusCode().is2xxSuccessful()).isTrue();
		
		DocumentContext context = JsonPath.parse(response.getBody());
		assertThat((String) context.read("$.username"))
			.isEqualTo(TEST_USERNAME);
	}
	
	@Test
	void verifyTest() {
		VerifyDTO body = new VerifyDTO(TEST_USERNAME, TEST_PASSWORD);
		ResponseEntity<String> response = rest.postForEntity("/accounts/verify", body, String.class);
		assertThat(response.getStatusCode().is2xxSuccessful()).isTrue();

		DocumentContext context = JsonPath.parse(response.getBody());
		assertThat((String) context.read("$.username"))
			.isEqualTo(TEST_USERNAME);
	}
	
	@Test
	@Disabled
	void removeAccountTest() {
		when(auth.getUserId()).thenReturn(3l);
		rest.delete("/accounts/"+3);
		assertThat(accountRepo.count()).isEqualTo(2l);
	}
	
	@Test
	void changePassowrdTest() {
		ChangePasswordDTO body = new ChangePasswordDTO();
		body.password = "user2abcd";
		body.passwordConfirm = "user2abcd";
		
		ResponseEntity<String> response = rest.postForEntity("/accounts/"+2+"/changePassword", body, String.class);
		assertThat(response.getStatusCode().is2xxSuccessful()).isTrue();
		
		VerifyDTO bodyVerify = new VerifyDTO("user2@flashcat.io", "user2abcd");
		response = rest.postForEntity("/accounts/verify", bodyVerify, String.class);
		assertThat(response.getStatusCode().is2xxSuccessful()).isTrue();
		
		body.password = "user2";
		body.passwordConfirm = "user2";
		response = rest.postForEntity("/accounts/"+2+"/changePassword", body, String.class);
		assertThat(response.getStatusCode().is2xxSuccessful()).isTrue();
	}
}
