package pl.pmajewski.flashcat.accounts.controller;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations="classpath:bootstrap-test.yml")
@ActiveProfiles("test")
class LoginControllerIntegrationTest {

	private static final String URL_LOGIN = "/login";
	
	@Autowired
	private TestRestTemplate rest;
	
	@Test
	void correctLoginData() {
		HttpHeaders auth = getAuthHeaders("user1@flashcat.io", "user1");
		ResponseEntity<String> response = rest.exchange(URL_LOGIN, HttpMethod.POST, new HttpEntity<>(auth), String.class);
		
		assertThat(response.getStatusCode().is2xxSuccessful()).isTrue();
	}
	
	@Test
	void invalidLoginData() {
		HttpHeaders auth = getAuthHeaders("xyz1234@flashcat.io", "kjadshfkj");
		ResponseEntity<String> response = rest.exchange(URL_LOGIN, HttpMethod.POST, new HttpEntity<>(auth), String.class);
		
		assertThat(response.getStatusCode().is4xxClientError()).isTrue();
	}
	
	private HttpHeaders getAuthHeaders(String login, String password) {
		HttpHeaders headers = new HttpHeaders();
		
		headers.add("username", login);
		headers.add("password", password);
		
		return headers;
	}
}
