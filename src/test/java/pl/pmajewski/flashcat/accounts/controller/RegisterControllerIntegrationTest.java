package pl.pmajewski.flashcat.accounts.controller;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

import pl.pmajewski.flashcat.accounts.dto.RegisterDTO;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations="classpath:bootstrap-test.yml")
@ActiveProfiles("test")
class RegisterControllerIntegrationTest {

	private static final String URL_REGISTER = "/register";
	
	@Autowired
	private TestRestTemplate rest;
	
	@Test
	void minimalCorrectData() {
		RegisterDTO body = RegisterDTO.builder()
			.username("test@test.com")
			.password("test1234")
			.build();
		
		ResponseEntity<String> response = rest.postForEntity(URL_REGISTER, body, String.class);
		String responseBody = response.getBody();
		DocumentContext context = JsonPath.parse(responseBody);
		
		int id = context.read("$.id");
		assertThat(id).isEqualTo(4);
	}
	
	@Test
	void invalidEmailTest() {
		RegisterDTO body = RegisterDTO.builder()
			.password("test1234")
			.build();
		
		ResponseEntity<String> response = rest.postForEntity(URL_REGISTER, body, String.class);
		String responseBody = response.getBody();
		DocumentContext context = JsonPath.parse(responseBody);
		
		int status = context.read("$.status");
		assertThat(status).isEqualTo(400);
	}
	
	@Test
	void emptyEmailTest() {
		RegisterDTO body = RegisterDTO.builder()
			.password("test1234")
			.build();
		
		ResponseEntity<String> response = rest.postForEntity(URL_REGISTER, body, String.class);
		String responseBody = response.getBody();
		DocumentContext context = JsonPath.parse(responseBody);
		
		int status = context.read("$.status");
		assertThat(status).isEqualTo(400);
	}
	
	@Test
	void emptyPasswordTest() {
		RegisterDTO body = RegisterDTO.builder()
			.username("testtest")
			.build();
		
		ResponseEntity<String> response = rest.postForEntity(URL_REGISTER, body, String.class);
		String responseBody = response.getBody();
		DocumentContext context = JsonPath.parse(responseBody);
		
		int status = context.read("$.status");
		assertThat(status).isEqualTo(400);
	}
}
