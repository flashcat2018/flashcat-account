package pl.pmajewski.flashcat.accounts.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

import pl.pmajewski.flashcat.accounts.dto.AccountDTO;
import pl.pmajewski.flashcat.accounts.exception.AccountNotFoundException;
import pl.pmajewski.flashcat.accounts.model.Account;
import pl.pmajewski.flashcat.accounts.repository.AccountRepository;
import pl.pmajewski.flashcat.accounts.utils.PasswordGenerator;

@SpringBootTest(classes = {AccountServiceImpl.class, PasswordGenerator.class})
@TestPropertySource(locations="classpath:bootstrap-test.yml")
@ActiveProfiles("test")
class AccountServiceImplIntegrationTest {

	@MockBean
	private AccountRepository repoMock;
	@Autowired
	private AccountServiceImpl subject;
	
	@Test
	void getById() {
		when(repoMock.findById(anyLong())).thenReturn(Optional.of(getFakeAccount()));
		AccountDTO account = subject.getById(1l);
		
		assertThat(account).isNotNull();
		assertThat(account.id).isEqualTo(1l);
	}
	
	@Test
	void getFromEmptyRepo() {
		when(repoMock.findById(anyLong())).thenReturn(Optional.empty());
		assertThatThrownBy(() -> subject.getById(1l))
			.isInstanceOf(AccountNotFoundException.class);
	}
	
	private Account getFakeAccount() {
		Account obj = new Account();
		
		obj.setId(1l);
		obj.setUsername("user1");
		obj.setPassword("9b48b01249c071fad3c5c09092906d34d89c4e362bced66b86521fb446eef380e5f2795d9b274d98125f2dc2c48fcf1d69e4aea9a60544307f207349c93c2c3b");
		obj.setPasswordSalt("uRzA2xAr9OCD5uSYyrgRICuyftxLAUhI2797dpIworY5CCLS4MZ1zZa5dCgZbA4acG6Gg7ufAj27F8jGXALYLefMICyCyp94IJxdrLerc6zBl5r84U8TgBzufbgFWspLHyCxR1KlShJXO2CpkyTXMlFABGXI4efC1W6k4ScWPYAkAFe6h7PHb8wc2ERt24uTE6JoDkD2xFPkzleI7pOaH24YikPtuHGkFbMXduPc8FEiCIjsAeR4DO4kOJxAGN92l3hy5ESzpkoMGf1Nt3TZJMF7oiPy1tzy2eGYsIiO6GopSFUERXRCluD3jEdS35IBKYb5clXhdFeIzuJ1XGgrPw5fUiCIAphhpb6jStNaGu82rFSofKZbpXg3hShUhloAArU77T2g2bStPMYYsy5iBlypllMeSAhlKF4IRP2FrKk8OLMOw1DPBuKKLHbUfDkgWa2XkjElkbeBEiiPBSkXx94CTswPgxwb8BC7YGkr6WAG55G1jwYlzoEzBXyN7To");
		obj.setFirstname("firstname1");
		obj.setLastname("lastname1");
		obj.setBirthday(LocalDateTime.of(1994, 11, 22, 0, 0));
		obj.setInsertTimestamp(LocalDateTime.now());
		
		return obj;
	}
}
